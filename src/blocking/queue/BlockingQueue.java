package blocking.queue;

public class BlockingQueue<T> {

    private T[] elements;

    private int limit;
    private int usedSlots = 0;

    public BlockingQueue() {
        this(10);
    }

    public BlockingQueue(int limit) {
        this.limit = limit;
        this.elements = (T[]) new Object[limit];
    }

    public synchronized void put(T t) {
        while (usedSlots == limit) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (this.usedSlots == 0) {
            this.notifyAll();
        }
        this.elements[usedSlots++] = t;
    }

    public synchronized T take() {
        while (this.usedSlots == 0) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (this.usedSlots == this.limit) {
            notifyAll();
        }

        return shiftElementsLeft();
    }

    private T shiftElementsLeft() {
        T popped = elements[0];
        for (int i = 0; i < usedSlots - 1; i++) {
            elements[i] = elements[i + 1];
        }
        usedSlots--;
        return popped;
    }

}
