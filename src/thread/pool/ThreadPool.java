package thread.pool;

import blocking.queue.BlockingQueue;

public class ThreadPool {

    private final int DEFAULT_THREAD_POOL_SIZE = 10;

    private PoolThread[] threads;
    private BlockingQueue<Runnable> queue;


    public ThreadPool() {
        threads = new PoolThread[DEFAULT_THREAD_POOL_SIZE];
        queue = new BlockingQueue<>();
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new PoolThread(queue);
            threads[i].start();
        }
    }

    public ThreadPool(int numberOfThreadsToStore) {
        threads = new PoolThread[numberOfThreadsToStore];
        queue = new BlockingQueue<>();
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new PoolThread(queue);
            threads[i].start();
        }
    }

    public void addToQueue(Runnable task) {
        queue.put(task);
    }

    public void down() {
        for (PoolThread thread : threads) {
            thread.interrupt();
        }
    }
}
