package thread.pool;

import blocking.queue.BlockingQueue;

public class PoolThread extends Thread {

    private BlockingQueue<Runnable> queue;

    public PoolThread(BlockingQueue<Runnable> queue) {
        this.queue = queue;
    }

    public void run() {
        while (!isInterrupted()) {
            this.queue.take().run();
        }
    }
}
